var isUniqueValues = function (arr) {
  let a = [];
  const uniq = ["1", "2", "3", "4", "4", "4", "7", "7", "12", "12", "13"];

  // looping
  for (let i = 0; i < arr.length; i++) {
    // condition have different value, arr push to a
    if (arr[i] !== arr[i + 1]) {
      a.push(arr[i]);
    }
  }
  return a.length;
};

console.log(isUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]));
console.log(isUniqueValues([1, 2, 2, 2, 2, 2, 2, 2, 4, 6]));
console.log(isUniqueValues([]));
